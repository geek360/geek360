# Geek360

Repositório de código HTML do projeto Geek360.

## Quem Somos?
O Geek360 é um blog de recomendação de produtos de tecnologia, aonde não só indicamos os melhores produtos mas também ensinamos as pessoas a comprarem o produto certo para suas necessidades através de nossos guias de compras. Venha ver nossas recomendações: https://geek360.com.br